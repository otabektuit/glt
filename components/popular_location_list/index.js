import { Container, Grid } from '@material-ui/core'
import React from 'react'
import styles from './popular_location_list.module.scss'

export default function PopularLocationList() {
  const locations = [
    {
      img:
        'https://images.musement.com/cover/0002/45/dubai-skyline-at-dusk-jpg_header-144981.jpeg',
      amount: '14 cars available',
      title: 'Dubai',
    },
    {
      img:
        'https://london.ac.uk/sites/default/files/styles/max_1300x1300/public/2018-10/london-aerial-cityscape-river-thames_1.jpg?itok=6LenFxuz',
      amount: '16 cars available',
      title: 'London',
    },
    {
      img:
        'https://images.adsttc.com/media/images/5d44/14fa/284d/d1fd/3a00/003d/large_jpg/eiffel-tower-in-paris-151-medium.jpg?1564742900',
      amount: '40 cars available',
      title: 'Paris',
    },
    {
      img:
        'https://liveberlin.ru/wp-content/uploads/2020/05/by-Subodh-on-AdobeStock_cover-scaled.jpeg',
      amount: '65 cars available',
      title: 'Berlin',
    },
    {
      img:
        'https://www.fodors.com/wp-content/uploads/2018/10/HERO_UltimateRome_Hero_shutterstock789412159.jpg',
      amount: '25 cars available',
      title: 'Rome',
    },
    {
      img:
        'https://turizm.world/wp-content/uploads/2015/04/korolevskui-madrid.jpg',
      amount: '35 cars available',
      title: 'Madrid',
    },
  ]
  return (
    <div className={styles.wrapper}>
      <Container>
        <div className={styles.block}>
          <h1 className='title'>Popular locations</h1>
          <Grid container spacing={3}>
            {locations.map((item) => (
              <Grid item xs={4}>
                <div className={styles.card}>
                  <div className={styles.card_img}>
                    <img src={item.img} />
                  </div>
                  <div className={styles.card_body}>
                    <h6>{item.title}</h6>
                    <p>{item.amount}</p>
                  </div>
                </div>
              </Grid>
            ))}
          </Grid>
        </div>
      </Container>
    </div>
  )
}
