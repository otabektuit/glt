import { Button, Container, Grid, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import cls from './hero.module.scss'
import PlaceOutlinedIcon from '@material-ui/icons/PlaceOutlined'
import GpsFixedOutlinedIcon from '@material-ui/icons/GpsFixedOutlined'
import LocalHospitalOutlinedIcon from '@material-ui/icons/LocalHospitalOutlined'
import MailOutlineOutlinedIcon from '@material-ui/icons/MailOutlineOutlined'
import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined'
import IndeterminateCheckBoxOutlinedIcon from '@material-ui/icons/IndeterminateCheckBoxOutlined'

function Input(props) {
  const {
    name,
    placeholder,
    type = 'text',
    id,
    value,
    htmlFor,
    onChange,
    children,
    isDate = false,
  } = props
  return (
    <label htmlFor={htmlFor} className={cls.label}>
      <input
        name={name}
        placeholder={placeholder}
        id={id}
        onChange={onChange}
        value={value}
        className={cls.input}
        type={type}
      />
      {children}
    </label>
  )
}

function Hero() {
  const address = { from_place: '', to_place: '', start_date: '', end_date: '' }
  const [data, setData] = useState({
    addresses: [{ from_place: '', to_place: '', start_date: '', end_date: '' }],
    car_class: '',
    email: '',
    passengers: '',
    tour_type: '',
  })

  const [car_classes, setCarClasses] = useState([
    {
      class: 'Economy',
      price_from: '100$',
    },
    {
      class: 'Business',
      price_from: '250$',
    },
    {
      class: 'Premium',
      price_from: '300$',
    },
  ])

  const [selectedCarClass, setSelectedCarClass] = useState(car_classes[0])

  const [tours, setTours] = useState(['Round Trip', 'Multi City Trip'])

  const [tour_type, setTourType] = useState(0)

  const handleChange = (index) => (ev) => {
    console.log(data)
    if (index >= 0) {
      let addresses = data.addresses
      addresses[index] = {
        ...addresses[index],
        [ev.target.name]: ev.target.value,
      }
      setData({
        ...data,
        addresses: addresses,
      })
    } else setData({ ...data, [ev.target.name]: ev.target.value })
  }

  const handleTourType = (type) => (e) => {
    if (type === 0) setData({ ...data, addresses: [data.addresses[0]] })
    setTourType(type)
  }

  const handleIncrement = (index) => (e) => {
    let addresses = data.addresses
    addresses.push(address)
    setData({ ...data, addresses: addresses })
  }
  const handleDecrement = (index) => (e) => {
    let addresses = data.addresses
    addresses.splice(index, 1)
    setData({ ...data, addresses: addresses })
  }

  return (
    <section className={cls.container}>
      <div className={cls.wrapper}>
        <Container>
          <Grid container>
            <Grid lg={12}>
              <h1 className={cls.title}>The car is waiting for you</h1>
              <p className={cls.about}>Read more information below</p>
            </Grid>
            <Grid lg={12}>
              <div className={cls.form}>
                <div className={cls.options}>
                  {tours.map((item, i) => (
                    <div
                      className={`${i === tour_type ? cls.active_option : ''} ${
                        cls.option
                      }`}
                      key={i}
                      onClick={handleTourType(i)}
                    >
                      {item}
                    </div>
                  ))}
                </div>
                <div className={cls.inputs}>
                  {data.addresses.map((item, i) => (
                    <div className={cls.form_group} key={i}>
                      {tour_type === tours.length - 1 ? (
                        i === 0 ? (
                          <span
                            className={`${cls.action_button_plus} ${cls.action_button}`}
                            onClick={handleIncrement(i)}
                          >
                            <LocalHospitalOutlinedIcon />
                          </span>
                        ) : (
                          <span
                            className={`${cls.action_button_minus} ${cls.action_button}`}
                            onClick={handleDecrement(i)}
                          >
                            <IndeterminateCheckBoxOutlinedIcon />
                          </span>
                        )
                      ) : (
                        ''
                      )}
                      <Input
                        htmlFor={'from_place'}
                        name={`from_place`}
                        id={`from_place`}
                        placeholder={`From Place`}
                        value={item.from_place}
                        onChange={handleChange(i)}
                      >
                        <PlaceOutlinedIcon />
                      </Input>

                      <Input
                        htmlFor={'to_place'}
                        name={`to_place`}
                        id={`to_place`}
                        placeholder={`To Place`}
                        value={item.to_place}
                        onChange={handleChange(i)}
                      >
                        <GpsFixedOutlinedIcon />
                      </Input>

                      <Input
                        htmlFor={'start_date'}
                        name={`start_date`}
                        id={`start_date`}
                        placeholder={`Arrival Date`}
                        value={item.start_date}
                        onChange={handleChange(i)}
                        type={`date`}
                        isDate={true}
                      ></Input>

                      <Input
                        htmlFor={'end_date'}
                        name={`end_date`}
                        id={`end_date`}
                        placeholder={`Last Date`}
                        value={item.end_date}
                        onChange={handleChange(i)}
                        type={`date`}
                        isDate={true}
                      ></Input>
                    </div>
                  ))}
                  <div className={cls.form_group}>
                    <Input
                      htmlFor={'passengers'}
                      name={`passengers`}
                      id={`passengers`}
                      placeholder={`Passengers`}
                      value={data.passengers}
                      onChange={handleChange(null)}
                      type={`number`}
                      isDate={true}
                    >
                      <SupervisorAccountOutlinedIcon />
                    </Input>
                    <Input
                      htmlFor={'email'}
                      name={`email`}
                      id={`email`}
                      placeholder={`Email`}
                      value={data.email}
                      onChange={handleChange(null)}
                      type={`mail`}
                      isDate={true}
                    >
                      <MailOutlineOutlinedIcon />
                    </Input>
                  </div>
                  <div className={cls.form_group}>
                    {car_classes.map((item, i) => (
                      <div
                        key={i}
                        className={`${
                          selectedCarClass.class === item.class
                            ? cls.car_option_active
                            : ''
                        } ${cls.car_option}`}
                        onClick={() => {
                          setSelectedCarClass(item)
                        }}
                      >
                        {item.class}
                      </div>
                    ))}
                  </div>
                </div>
                <div className={cls.actions}>
                  <p>
                    Prices Start from <h1>{selectedCarClass.price_from}</h1>
                  </p>
                  <Button fullWidth color={`secondary`}>
                    Get A Quote
                  </Button>
                </div>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    </section>
  )
}

export default Hero
