import { Container, Grid } from '@material-ui/core'
import React from 'react'
import styles from './advantages.module.scss'
import ContactPhoneOutlinedIcon from '@material-ui/icons/ContactPhoneOutlined'
import EventNoteOutlinedIcon from '@material-ui/icons/EventNoteOutlined'
export default function Advantages() {
  return (
    <div className={styles.wrapper}>
      <Container>
        <div className={styles.block}>
          <h1 className='title'>Advantages</h1>
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <div className={styles.card}>
                <div className={styles.card_icon}>
                  <img src='/images/operator.svg' alt='operator' />
                </div>
                <div className={styles.card_body}>
                  <h6>24/7 Customer online support</h6>
                  <p>Call us Anywhere Anytime</p>
                </div>
              </div>
            </Grid>
            <Grid item xs={4}>
              <div className={styles.card}>
                <div className={styles.card_icon}>
                  <img src='/images/calendar.svg' alt='calendar' />
                </div>
                <div className={styles.card_body}>
                  <h6>Reservation Anytime You Wants</h6>
                  <p>24/7 Online reservation</p>
                </div>
              </div>
            </Grid>
            <Grid item xs={4}>
              <div className={styles.card}>
                <div className={styles.card_icon}>
                  <img src='/images/location.svg' alt='location' />
                </div>
                <div className={styles.card_body}>
                  <h6>Lost of Picking Locations</h6>
                  <p>250+ locations</p>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </Container>
    </div>
  )
}
