import { Container, Grid } from '@material-ui/core'
import React from 'react'
import LanguagePicker from '../LanguagePicker'
import cls from './header.module.scss'

function Header() {
  return (
    <header className={cls.container}>
      <Container>
        <Grid container>
          <div className={cls.wrapper}>
            <a href={`#`}>CARRENT</a>
            <ul className={cls.list}>
              <li className={cls.list_item}>
                <a
                  href="email:otabektuit11@gmail.com"
                  className={cls.contact_mail}
                >
                  otabektuit11@gmail.com
                </a>
                <a href="tel:+998998154828" className={cls.contact_tel}>
                  +998 (99) 815-48-28
                </a>
              </li>
              <li className={cls.list_item}>
                <LanguagePicker />
              </li>
            </ul>
          </div>
        </Grid>
      </Container>
    </header>
  )
}

export default Header
