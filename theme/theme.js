import { createMuiTheme } from '@material-ui/core/styles'
import { red } from '@material-ui/core/colors'

// Create a theme instance.
const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
          backgroundColor: '#fff',
          fontSize: '20px',
          lineHeight: '18px',
          color: ' #0F1826',
          fontFamily: 'Helvetica',
        },
      },
    },
    MuiContainer: {
      root: {
        paddingLeft: '15px',
        paddingRight: '15px',
        '@media (min-width: 600px)': {
          paddingLeft: '15px',
          paddingRight: '15px',
        },
      },
      maxWidthLg: {
        '@media (min-width: 1280px)': {
          maxWidth: '1160px',
        },
        '@media (min-width: 1499px)': {
          maxWidth: '1366px',
        },
      },
    },
    MuiGrid: {
      // root: {
      //   '@media (max-width: 600px)': {
      //     padding: '0!important',
      //   },
      // },
      // 'spacing-xs-4': {
      //   '@media (max-width: 600px)': {
      //     width: '100%',
      //     margin: 0,
      //   },
      // },
      // 'spacing-xs-3': {
      //   '@media (max-width: 600px)': {
      //     width: '100%',
      //     margin: 0,
      //   },
      // },
    },
    MuiFilledInput: {
      root: {
        backgroundColor: '#F0EFEF',
      },
    },
    MuiOutlinedInput: {
      root: {
        backgroundColor: 'var(--secondary-color)',
        fontSize: '20px',
        lineHeight: '24px',
        letterSpacing: '0.05em',
      },
      notchedOutline: {
        borderColor: 'var(--secondary-color)',
        borderWidth: '1px !important',
      },
      input: {
        padding: '19.5px 16px',
      },
      multiline: {
        backgroundColor: '#FFF',
      },
    },
    MuiTextField: {
      root: {},
    },
    MuiCircularProgress: {
      colorPrimary: {
        color: '#ae8d5e',
      },
    },
    // MuiCircularProgress: { colorPrimary: '#ae8d5e!important' },
    MuiButton: {
      root: {
        textTransform: 'lowercase',
        padding: '12px 30px;',
        fontSize: '20px',
        lineHeight: '25px',
        letterSpacing: '0.05em',
        height: '60px',
        fontWeight: '500',
        transition: 'all .2s ease-in-out',
        borderRadius: '4px',
        '&:hover': {
          transform: 'translateY(-1px)',
        },
      },
      fullWidth: {
        maxWidth: '100%',
      },
      sizeSmall: {
        fontSize: '16px',
        lineHeight: '21px',
        padding: '8px 48px',
        textTransform: 'none',
      },
      sizeLarge: {
        fontSize: '20px',
        lineHeight: '26px',
        letterSpacing: '0.06em',
        padding: '20px 56px',
        textTransform: 'none',
      },
      outlinedPrimary: {
        '&:hover': {
          backgroundColor: 'transparent',
        },
      },
      contained: {
        boxShadow: 'none',
        '&:hover, &:active': {
          boxShadow: 'none',
        },
      },
      containedSecondary: {
        '&:hover': {
          // backgroundColor: '#fff',
        },
      },
      // containedPrimary: {
      //   '&:hover': {
      //     backgroundColor: '#fff',
      //   },
      // },
    },
    MuiFormControlLabel: {
      root: {
        display: 'flex',
        // flexDirection: 'row-reverse',
        // justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        margin: 0,
        padding: '0',
        borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
        '&:last-child': {
          borderBottom: 'none',
        },
      },
    },
    MuiCheckbox: {
      root: {
        '&$checked': {
          '&:hover': {
            backgroundColor: 'transparent !important',
          },
        },
        '&:hover': {
          backgroundColor: 'transparent !important',
        },
      },
    },
    MuiIconButton: {
      root: {
        borderRadius: 0,
        fontSize: '20px',
        padding: '6px !important',
        textTransform: 'uppercase',
      },
    },

    MuiTabs: {
      root: {
        display: 'block',
      },
    },

    MuiInputLabel: {
      root: {
        backgroundColor: 'transparent!important',
        // color: '#0F1826!important',
        color: '#0F1826!important',
        fontSize: '18px',
        fontWeight: '800',
        opacity: '0.7',
      },
      outlined: {
        color: 'var(--dark-color)!important',
      },
    },
    MuiDialog: {
      // Name of the rule
      paper: {
        width: '70%',
        padding: '0px 0 30px 0',
      },
      paperWidthSm: {
        maxWidth: '100%',
      },
    },
    PrivateNotchedOutline: {
      root: {
        border: 'none!important',
      },
      legendLabelled: {
        minWidth: '100px',
        color: 'var(--primary-color)',
      },
    },
  },
  palette: {
    primary: {
      main: 'rgb(36, 114, 187)',
    },
    secondary: {
      main: '#dd541e',
    },
    error: {
      main: red.A400,
    },
    background: {
      main: '#F9F4E8',
    },
  },
  typography: {
    fontFamily: 'Helvetica',
    body1: {
      fontSize: '20px',
      lineHeight: '24px',
      color: 'primary',
    },
    body2: {
      fontFamily: 'Helvetica',
      fontSize: '20px',
    },
  },
  shape: {
    borderRadius: 0,
  },
  spacing: 8,
  props: {
    MuiButton: {
      disableRipple: true,
      variant: 'contained',
      color: 'primary',
    },
    MuiSvgIcon: {
      color: 'primary',
      fontSize: '18px',
    },
    MuiCheckbox: {
      disableRipple: true,
      color: 'primary',
      size: 'small',
    },
    MuiTextField: {
      variant: 'outlined',
      InputLabelProps: {
        shrink: true,
      },
      color: 'primary',
    },
    MuiPaper: {
      elevation: 0,
    },
    MuiCard: {
      elevation: 0,
    },
    MuiTabs: {
      disableRipple: false,
      disableFocusRipple: false,
    },
    MuiCircularProgress: {
      circle: {
        strokeWidth: '6px!important',
      },
    },
  },
})

export default theme
