const { nextI18NextRewrites } = require('next-i18next/rewrites')

const localeSubpaths = {
  uz: 'uz',
  ru: 'ru',
}
const BASE_URL = 'https://api.najottalim.uz/'
module.exports = {
  env: {},
  rewrites: async () => nextI18NextRewrites(localeSubpaths),
  publicRuntimeConfig: {
    localeSubpaths,
  },
}
