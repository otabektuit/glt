import Head from 'next/head'
import Advantages from '../components/advantages'
import Hero from '../components/hero/hero'
import PopularLocationList from '../components/popular_location_list'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <>
      <Hero />
      <PopularLocationList />
      <Advantages />
    </>
  )
}
